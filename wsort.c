#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char **lines;
size_t nlines = 0;
size_t lines_allocated = 0;

// Initialize every char pointer of the ptr array to NULL
static void init_zero(char **ptr, size_t n) {
    while (n--) {
        *ptr = NULL;
        ptr++;
    }
}

// Free all individual lines and finally the char pointer array itself
static void free_lines(char **lines, size_t nlines) {
    char **line_iter = lines;
    while (nlines--) {
        free(*line_iter);
        line_iter++;
    }
    free(lines);
}

// Wrapper around strcmp to be used as a compare function
static int strcmp_wrapper(const void *lhs, const void *rhs) {
    return strcmp(* (const char * const *) lhs, * (const char * const *) rhs);
}

int main(void)
{
    lines_allocated = 8;
    lines = (char**) calloc(lines_allocated, sizeof(char*));
    if (!lines) {
        perror("calloc");
        exit(1);
    }
    ssize_t line_len;
    size_t buff_size = 0;
    size_t i = 0;

    // Read all lines from stdin into the lines array
    while ((line_len = getline(lines+i, &buff_size, stdin)) != -1) {
        if (line_len > 0 && lines[i][line_len-1] == '\n') {
            // Strip trailing newline
            lines[i][line_len-1] = '\0';
            line_len--;
        }
        bool too_long = line_len > 100;
        if (too_long) {
            fputs("Warning: this line is longer than 100 characters, ignoring this line.\n", stderr);
        }
        if (too_long || line_len == 0) { // Ignore empty and too long lines
            free(lines[i]);
            lines[i] = NULL;
        }
        else {
            i++;
            // Reallocate, if a larger line array is needed
            if (i >= lines_allocated) {
                size_t prev_allocated = lines_allocated;
                lines_allocated *= 2;
                char** new_lines = (char**) realloc(lines, lines_allocated * sizeof(char*));
                if (!new_lines) {
                    free_lines(lines, prev_allocated);
                    perror("realloc");
                    exit(1);
                }
                lines = new_lines;
                init_zero(lines + prev_allocated, lines_allocated - prev_allocated);
            }
        }
        buff_size = 0;
    }
    if (ferror(stdin)) {
        free_lines(lines, lines_allocated);
        perror("getline");
        exit(1);
    }
    size_t nlines = i;

    // Sort the lines alphabetically
    qsort(lines, nlines, sizeof(char*), strcmp_wrapper);

    // Print the sorted lines
    for (size_t i = 0; i < nlines; i++) {
        if (puts(lines[i]) == EOF) {
            free_lines(lines, lines_allocated);
            perror("puts");
            exit(1);
        }
    }

    // Free allocated memory
    free_lines(lines, lines_allocated);
    return 0;
}
